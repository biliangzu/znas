#znas
## 1. 下载源码
	可以先看看：
	http://www.nslu2-linux.org/wiki/Optware/AddAPackageToOptware
	然后使用svn clone最新代码：
	http://svn.nslu2-linux.org/svnroot/optware/trunk optware
	编译环境需要linux 平台，最好clone到linux下的目录,我的是/datadisk/znas/
## 2 准备文件
	2.1 拷贝toolchain-znas.mk packages-znas.mk到\optware-master\platforms\中
	2.2 进入http://pan.baidu.com/s/1o63pK5G ,下载交叉编译工具: cross-sdk3.2rc1-ct-ng-1.4.1-arm-mv5sft-linux-gnueabi-1107-12-07-2009.tar.bz2
	2.3 拷贝 cross-sdk3.2rc1-ct-ng-1.4.1-arm-mv5sft-linux-gnueabi-1107-12-07-2009.tar.bz2 到/datadisk/znas/optware-master/downloads中
	
## 3 编译交叉工具
	3.1 cd  /datadisk/znas/optware-master
	3.2	make znas-target
	3.3 cd znas
	3.4	make directories ipkg-utils
	3.5 make toolchain
	3.6 make hello-ipk 测试
	
## 4 znas 上安装，运行程序
	4.1 ssh 登录铁威马
	4.2 mount -o remount rw /  #使flash可写
	4.3 cp /mnt/public/hello_2.7-1_arm.ipk   /   #复制软件包到/下，默认安装是根据根目录为路径的
	4.4 unpkg hello_2.7-1_arm.ipk 
	4.5 执行hello ,显示Hello, world!，即表示成功