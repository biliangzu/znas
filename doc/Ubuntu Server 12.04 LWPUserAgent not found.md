环境：

Ubuntu Server 12.04 x64

Cacti-0.8.8a

今天安装了Cacti监控，安装完成后，导入Nginx监控模板，发现监控的时候，一直不出图，很是郁闷。结果在系统时面直接调用监控Nginx状态的pl脚本，发现报错

cactiuser@00098:/var/tmp/cacti/cacti/rra$ /usr/bin/perl /var/tmp/cacti/cacti-0.8.8a/scripts/get_nginx_clients_status.pl   http://192.168.1.1:7039/status 

no (LWP::UserAgent not found) 

网上找了一下资料，是说perl的组件不全，使用命令安装

# perl -MCPAN -e shell

直到出现cpan[1]> 提示符，然后再输入install LWP::UserAgent

这个过程很漫长，并且我照这种方法，也没成功。

解决方案：

虽然照上面的方法没成功，但是却看到了意外的惊喜，发现少的包是libwww-perl

结果直接apt-get安装，还真的就搞定了

# sudo apt-get install libwww.perl

本篇文章来源于 Linux公社网站(www.linuxidc.com)  原文链接：http://www.linuxidc.com/Linux/2012-07/65556.htm